package utils

import (
	"encoding/json"
	"net/http"
)

func ErrCode(c string) string {
	switch c {
	case "H404":
		return "Not Found"
	case "H500":
		return "Internal Server Error"
	}
	return c
}

func ResJson(w http.ResponseWriter, data map[string]interface{}, status int) {
	res := map[string]interface{}{
		"status": status,
	}
	for key, value := range data {
		res[key] = value
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(res)
}

func ResStruct(w http.ResponseWriter, data interface{}, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(data)
}

func ResSuccess(w http.ResponseWriter) {
	res := struct {
		Description string `json:"description"`
	}{
		Description: "success",
	}
	ResStruct(w, res, http.StatusOK)
}

func ResErr(w http.ResponseWriter, err error, errCode string, status int) {
	res := struct {
		ErrorCode        string `json:"errCode"`
		ErrorDescription string `json:"errDes"`
		Err              string `json:"err"`
	}{
		ErrorCode:        errCode,
		ErrorDescription: ErrCode(errCode),
		Err:              err.Error(),
	}
	ResStruct(w, res, status)
}

func ResNotFound(w http.ResponseWriter, item string) {
	res := map[string]interface{}{
		"errCode": "H404",
		"errDes":  ErrCode("H404"),
		"err":     item + " " + ErrCode("H404"),
	}
	ResJson(w, res, 404)
}
