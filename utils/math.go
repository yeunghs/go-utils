package utils

import (
	"fmt"
	"math/big"
	"strconv"
)

func FloatToInt(val float64) *big.Int {
	bigval := new(big.Float)
	bigval.SetFloat64(val)
	// Set precision if required.
	// bigval.SetPrec(64)

	coin := new(big.Float)
	coin.SetInt(big.NewInt(1000000000000000000))

	bigval.Mul(bigval, coin)

	result := new(big.Int)
	bigval.Int(result) // store converted number in result

	return result
}

func floatToString(value float64) string {
	return strconv.FormatFloat(value, 'f', -1, 64)
}

// Convert string to float64
func stringToFloat(value string) (float64, error) {
	return strconv.ParseFloat(value, 64)
}

func Add(a, b float64, decimals int) (float64, error) {
	// Convert float64 to big.Float
	ba, successA := new(big.Float).SetString(floatToString(a))
	bb, successB := new(big.Float).SetString(floatToString(b))
	if !successA || !successB {
		return 0, fmt.Errorf("error converting float64 to big.Float")
	}

	// Perform addition
	result := new(big.Float).Add(ba, bb)

	// Set precision and return as float64
	resultStr := result.Text('f', decimals)
	return stringToFloat(resultStr)
}

// Subtract two float64 numbers with a specified number of decimal places
func Sub(a, b float64, decimals int) (float64, error) {
	// Convert float64 to big.Float
	ba, successA := new(big.Float).SetString(floatToString(a))
	bb, successB := new(big.Float).SetString(floatToString(b))
	if !successA || !successB {
		return 0, fmt.Errorf("error converting float64 to big.Float")
	}

	// Perform subtraction
	result := new(big.Float).Sub(ba, bb)

	// Set precision and return as float64
	resultStr := result.Text('f', decimals)
	return stringToFloat(resultStr)
}

// Multiply two float64 numbers with a specified number of decimal places
func Mul(a, b float64, decimals int) (float64, error) {
	// Convert float64 to big.Float
	ba, successA := new(big.Float).SetString(floatToString(a))
	bb, successB := new(big.Float).SetString(floatToString(b))
	if !successA || !successB {
		return 0, fmt.Errorf("error converting float64 to big.Float")
	}

	// Perform multiplication
	result := new(big.Float).Mul(ba, bb)

	// Set precision and return as float64
	resultStr := result.Text('f', decimals)
	return stringToFloat(resultStr)
}

// Divide two float64 numbers with a specified number of decimal places
func Div(a, b float64, decimals int) (float64, error) {
	// Convert float64 to big.Float
	ba, successA := new(big.Float).SetString(floatToString(a))
	bb, successB := new(big.Float).SetString(floatToString(b))
	if !successA || !successB {
		return 0, fmt.Errorf("error converting float64 to big.Float")
	}

	// Perform division
	result := new(big.Float).Quo(ba, bb)

	// Set precision and return as float64
	resultStr := result.Text('f', decimals)
	return stringToFloat(resultStr)
}
