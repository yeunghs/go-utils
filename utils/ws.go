package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"

	"github.com/gorilla/websocket"
)

func SendJsonMessage(ws *websocket.Conn, message map[string]interface{}) {
	messageJson, err := json.Marshal(message)
	if err != nil {
		res := map[string]interface{}{
			"err":         "SendJsonMessage Error",
			"description": "covert message to json format failed",
		}
		messageJson, _ = json.Marshal(res)
		return
	}
	if err := ws.WriteMessage(1, messageJson); err != nil {
		return
	}
	// log that message was sent
	fmt.Println("server message sent:")
	ConsoleLogJsonByteArray(messageJson)
}

func ConsoleLogJsonByteArray(message []byte) {
	var out bytes.Buffer
	json.Indent(&out, message, "", "  ")
	out.WriteTo(os.Stdout)
}

func WsError(err error) map[string]interface{} {
	var WsError = map[string]interface{}{
		"messageType": "SERVER_MESSAGE_TYPE_ERROR",
		"err":         err.Error(),
	}
	return WsError
}
