package utils

import (
	"time"
)

func MapToString(obj map[string]interface{}, key string) *string {
	if obj[key] == nil {
		return nil
	}
	if str, ok := obj[key].(string); ok {
		return &str
	}
	return nil
}

func MapToStringArr(obj map[string]interface{}, key string) []string {
	if obj[key] == nil {
		return nil
	}
	aInterface := obj[key].([]interface{})
	aString := make([]string, len(aInterface))
	for i, v := range aInterface {
		str, ok := v.(string)
		if !ok {
			return nil
		}
		aString[i] = str
	}
	return aString
}

func MapToInt(obj map[string]interface{}, key string) *int {
	if obj[key] == nil {
		return nil
	}
	if float, ok := obj[key].(float64); ok {
		integer := int(float)
		return &integer
	}
	return nil
}

func MapToIntArr(obj map[string]interface{}, key string) []int {
	if obj[key] == nil {
		return nil
	}
	aInterface := obj[key].([]interface{})
	aInt := []int{}
	for i, v := range aInterface {
		intt, ok := v.(float64)
		if !ok {
			return nil
		}
		aInt[i] = int(intt)
	}
	return aInt
}

func MapToFloat(obj map[string]interface{}, key string) *float64 {
	if obj[key] == nil {
		return nil
	}
	if float, ok := obj[key].(float64); ok {
		return &float
	}
	return nil
}

func MapToFloatArr(obj map[string]interface{}, key string) []float64 {
	if obj[key] == nil {
		return nil
	}
	aInterface := obj[key].([]interface{})
	afloat := []float64{}
	for i, v := range aInterface {
		intt, ok := v.(float64)
		if !ok {
			return nil
		}
		afloat[i] = intt
	}
	return afloat
}

func MapToBool(obj map[string]interface{}, key string) *bool {
	if obj[key] == nil {
		return nil
	}
	if boolean, ok := obj[key].(bool); ok {
		return &boolean
	}
	return nil
}

func MapToTime(obj map[string]interface{}, key string) *time.Time {
	if obj[key] == nil {
		return nil
	}
	if timestamp, ok := obj[key].(float64); ok {
		time := time.UnixMilli(int64(timestamp))
		return &time
	}
	return nil
}

func MapToMap(obj map[string]interface{}, key string) *map[string]interface{} {
	if obj[key] == nil {
		return nil
	}
	if subObj, ok := obj[key].(map[string]interface{}); ok {
		return &subObj
	}
	return nil
}

func MapToArrMap(obj map[string]interface{}, key string) *[]map[string]interface{} {
	if obj[key] == nil {
		return nil
	}
	if intfArr, ok := obj[key].([]interface{}); ok {
		var arr []map[string]interface{}
		for _, value := range intfArr {
			arr = append(arr, value.(map[string]interface{}))
		}
		return &arr
	}
	return nil
}

func MapTo2dStringArr(obj map[string]interface{}, key string) *[][]string {
	if obj[key] == nil {
		return nil
	}
	intf2dArr, ok := obj[key].([]interface{})
	if !ok {
		return nil
	}
	string2dArr := [][]string{}
	for _, intf := range intf2dArr {
		intfArr, ok := intf.([]interface{})
		if !ok {
			continue
		}
		stringArr := []string{}
		for _, val := range intfArr {
			if value, ok := val.(string); ok {
				stringArr = append(stringArr, value)
			}
		}
		string2dArr = append(string2dArr, stringArr)
	}
	return &string2dArr
}
