package utils

import (
	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

func SignToAddress(sign, message string) (string, error) {
	msgByte := []byte(message)
	sig := hexutil.MustDecode(sign)

	msg := accounts.TextHash(msgByte)
	sig[crypto.RecoveryIDOffset] -= 27 // Transform yellow paper V from 27/28 to 0/1

	recovered, err := crypto.SigToPub(msg, sig)
	if err != nil {
		return "", nil
	}

	recoveredAddr := crypto.PubkeyToAddress(*recovered)
	return recoveredAddr.String(), nil
}
