package utils

import "errors"

func Catch(logger *Logger) {
	if err := recover(); err != nil {
		logger.Error().Msgf("Error: ", err)
	}
}

func Contains(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}
	_, ok := set[item]
	return ok
}

func RemoveElement(slice []string, element string) ([]string, error) {
	var i = -1
	for k, v := range slice {
		if element == v {
			i = k
			break
		}
	}
	if i == -1 {
		return []string{}, errors.New("element is not in slice")
	}
	return append(slice[:i], slice[i+1:]...), nil
	// slice[i] = slice[len(slice)-1]
	// return slice[:len(slice)-1], nil
}
